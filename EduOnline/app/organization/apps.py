from django.apps import AppConfig


class OriganzationConfig(AppConfig):
    name = 'organization'
    verbose_name = '组织机构信息'

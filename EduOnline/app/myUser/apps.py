from django.apps import AppConfig


class MyuserConfig(AppConfig):
    name = 'myUser'
    verbose_name = '用户信息'